---@enum weapon
local WEAPON = {
    bow = "bow",
    crossbow = "crossbow",
    spear = "spear",
    pike = "pike",
    mace = "mace",
    sword = "sword",
    leatherArmor = "leatherArmor",
    shield = "shield",
}

return WEAPON
