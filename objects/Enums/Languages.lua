---@enum language
local LANGUAGES = {
    ENG = "ENG",
    FRA = "FRA",
    DEU = "DEU",
    ITA = "ITA",
    ESP = "ESP",
    POL = "POL",
    JPN = "JPN",
    BRA = "BRA",
    CHN = "CHN",
    RUS = "RUS",
    CZE = "CZE",
    POR = "POR",
}

return LANGUAGES
