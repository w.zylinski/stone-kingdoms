local game = {}
local loveframes = require("libraries.loveframes")
local ActionBar = require("states.ui.ActionBar")
local states = require("states.ui.states")
local core = require("misc")
local thread, thread2, objects
---@type Terrain
local Terrain
require("shaders.postshader")
local renderLoadingScreen = require("states.ui.loading_screen")
local renderLoadingBar = require("states.ui.loading_bar")
local loadState, progress = 1, 15
local SaveManager = require("objects.Controllers.SaveManager")
local keybindManager = require("objects.Controllers.KeybindManager")
local EVENT = require("objects.Enums.KeyEvents")
local savegame
local playlist = require("sounds.music_playlist")
local RationController
local groupTypeMarket = require("states.ui.market.market_trade_main")
local ArmouryUI = require("states.ui.armoury.armoury_ui")
local _, MarketUI = unpack(require("states.ui.market.market_trade"))
local BarracksUI = require("states.ui.barracks.units_recruitment")
local GuildsUI = require("states.ui.guilds.guild_ui")
local WorkshopsUI = require("states.ui.workshops.workshops_ui")
local UnitsUI = require("states.ui.units.units_control")
local console = require "libraries.console"

local function updateProgress(prgs, lState)
    progress = prgs or progress
    loadState = lState or loadState
    game:draw()
    love.graphics.present()
end

local function delayedInit()
    updateProgress(20)
    objects = love.filesystem.load("objects/objects.lua")(objectAtlas)
    package.loaded["objects.objects"] = objects
    updateProgress(30)
    _G.BrushController = require("objects.Controllers.BrushController")
    _G.DestructionController = require("objects.Controllers.DestructionController"):new()
    _G.SleepController = require("objects.Controllers.SleepController"):new()
    RationController = require("objects.Controllers.RationController")
    _G.TaxController = require("objects.Controllers.TaxController")
    _G.TimeController = require("objects.Controllers.TimeController")
    _G.MissionController = require("objects.Controllers.MissionController")
    _G.PopularityController = require("objects.Controllers.PopularityController")
    _G.ScribeController = require("objects.Controllers.ScribeController")
    _G.BuildController = love.filesystem.load("objects/Controllers/BuildController.lua")(
        package.loaded["objects.objects"].object, objectAtlas)
    _G.JobController = require("objects.Controllers.JobController")
    _G.BuildingManager = require("objects.Controllers.BuildingManager")
    _G.Commander = require("objects.Controllers.Commander")
    _G.DebugView = require("objects.Controllers.DebugView")
    updateProgress(35)
    ----Pathfinding setup
    updateProgress(40)
    _G.finder = require("objects.Controllers.PathController")
    _G.state.newGame = savegame == "map_Fernhaven"
    if _G.state.newGame then
        SaveManager:load(savegame)
        updateProgress(70)
    else
        updateProgress(70, 3)
        SaveManager:load(savegame)
    end
    core.update()
    updateProgress(80, 4)
    objects.update(_G.dt)
    updateProgress(90, 5)
    _G.state.Terrain:update()
    updateProgress(95)
    _G.BuildController:update()
    loveframes.update()
    _G.finder:update()
    updateProgress(97)
    _G.state.map:forceRefresh()
    _G.state.Terrain:update()
    updateProgress(100)
    love.timer.sleep(0.4)
    if _G.state.missionNr then
        _G.MissionController:setMissionState(_G.state.missionNr)
    end
    loveframes.SetState(states.STATE_INGAME_CONSTRUCTION)
    ActionBar:updateGoldCount()
    ActionBar:updatePopularityCount()
    _G.state:shadeBuildings()
    _G.loaded = true
    _G.speedModifier = 1
    if _G.state.newGame then
        _G.playSpeech("place_a_keep")
        _G.BuildController.start = true
        ActionBar:showGroup("start")
        local buttons = require("states.ui.construction.level_1_new_game")
        buttons.castleButton:enable()
        buttons.stockpileButton:disable()
        buttons.granaryButton:disable()
    else
        ActionBar:showGroup("main")
    end
    _G.paused = false
    loveframes.SetState(states.STATE_INGAME_CONSTRUCTION)
end

function game:init()
end

local scrolledAmountWithinShortPeriod = 0
local scrollCountDown = 0.05
function game:update(dt)
    if (not _G.state or not _G.state.initialized) then
        delayedInit()
        _G.state.initialized = true
    else
        prof.push("core")
        core.update()
        if scrollCountDown > 0 then
            scrollCountDown = scrollCountDown - love.timer.getDelta()
        elseif scrollCountDown < 0 then
            scrollCountDown = 0
            core.scale(scrolledAmountWithinShortPeriod)
        end
        prof.pop("core")
        if not _G.paused then
            local HighlightView = require("objects.Controllers.HighlightView")
            prof.push("objects")
            objects.update(dt)
            prof.pop("objects")
            _G.state.Terrain:update()
            prof.push("bcontr")
            HighlightView:update()
            _G.BuildController:update()
            _G.DebugView:update()
            _G.BrushController:update()
            if not _G.BuildController.start then
                _G.TimeController:update()
                RationController:update()
                _G.TaxController:update()
                _G.PopularityController:update()
                _G.ScribeController:update()
                _G.DestructionController:update()
                _G.SleepController:update()
                _G.MissionStart = true
                if (loveframes.GetState() == states.STATE_ARMOURY) then
                    ArmouryUI.DisplayCurrentStock()
                end
                if (loveframes.GetState() == states.STATE_MARKET) then
                    MarketUI(groupTypeMarket.name)
                end
                if (loveframes.GetState() == states.STATE_BARRACKS) then
                    BarracksUI.DisplayCurrentStock()
                end
                if (loveframes.GetState() == states.STATE_GUILDS) then
                    GuildsUI.DisplayButtons()
                end
                if (loveframes.GetState() == states.STATE_INGAME_CONSTRUCTION) then
                    WorkshopsUI.CheckTooltip()
                end
            end
            prof.pop("bcontr")
        end
        prof.push("ui")
        loveframes.update()
        prof.pop("ui")
        prof.push("pathfind")
        _G.finder:update()
        prof.pop("pathfind")
        local error = _G.state.thread:getError()
        assert(not error, error)
        error = _G.state.thread2:getError()
        assert(not error, error)
        if not _G.BuildController.start then
            playlist()
        end
    end
end

function game:enter(_, savegameName)
    love.graphics.setBackgroundColor(26 / 255, 26 / 255, 26 / 255, 1)
    savegame = savegameName
    collectgarbage()
    collectgarbage()
    if _G.loaded then
        _G.paused = false
        loveframes.SetState(states.STATE_INGAME_CONSTRUCTION)
    end
end

function game:draw()
    if not _G.testMode then
        if _G.loaded then
            local HighlightView = require("objects.Controllers.HighlightView")
            if _G.state.scaleX >= 2.1 or _G.paused then
                love.postshader.setBuffer("render")
            end
            love.graphics.push()
            love.graphics.translate((love.graphics.getWidth() / 2), (love.graphics.getHeight() / 2))
            objects.draw()
            if not _G.paused then
                HighlightView:draw()
                _G.BuildController:draw()
                _G.DebugView:draw()
                _G.BrushController:draw()
            end
            _G.Commander:draw()
            love.graphics.pop()
            if _G.paused then
                love.postshader.addTiltshift(12)
            elseif _G.state.scaleX >= 2.1 then
                love.postshader.addTiltshift(4)
            end
            core.draw()
            prof.push("ui_draw")
            loveframes.draw()
            prof.pop("ui_draw")
            if not _G.paused then
                _G.ScribeController:draw()
            end
            if _G.state.scaleX >= 2.1 or _G.paused then
                love.postshader.draw()
            end
            _G.Commander:drawMouse()
            if not _G.paused then
                local WallController = require("objects.Controllers.WallController")
                WallController:drawMouse()
            end
            console.draw()
        else
            renderLoadingScreen("")
            renderLoadingBar(loadState, progress)
            loveframes.draw()
        end
    end
end

function game:mousepressed(x, y, button, istouch)
    if not _G.loaded then return end
    if loveframes.mousepressed(x, y, button) then
        return
    end
    if _G.paused then return end
    if objects.mousepressed(x, y, button, istouch) then
        return
    end
    if _G.Commander:mousepressed(x, y, button) then
        return
    end
    if button == 2 then
        if not _G.BuildController.start then
            _G.BuildController:disable()
            local WallController = require("objects.Controllers.WallController")
            WallController.clicked = false
            if _G.BuildController.onBuildCallback then
                _G.BuildController.onBuildCallback()
                _G.BuildController.onBuildCallback = nil
            end
        end
        if not _G.BuildController.start and (loveframes.GetState() ~= states.STATE_INGAME_CONSTRUCTION or not ActionBar.hasSelectedButton) then
            if #_G.Commander.selectedUnits <= 1 then
                ActionBar:switchMode()
            end
        else
            ActionBar:unselectAll()
        end
    end
    _G.BrushController:mousepressed(button)
end

function game:textinput(text)
    console.textinput(text)
end

function game:keypressed(key, scancode, isRepeat)
    if love.keyboard.isScancodeDown("`") and love.keyboard.isScancodeDown("lshift") then
        console:toggleEnable()
        return
    end
    if console.isEnabled() then
        console.keypressed(key, scancode, isRepeat)
        return
    end
    ActionBar:keypressed(key, scancode)

    local event = keybindManager:getEventForKeypress(key)

    if event == EVENT.Screenshot then
        -- Screenshot
        local filename = string.format("%s_%d.png", _G.version, os.time())
        love.graphics.captureScreenshot(filename)
        print(string.format("Screenshot [%s] saved in [%s]", filename, love.filesystem.getSaveDirectory()))
    elseif event == EVENT.IncreaseGameSpeed then
        if _G.speedModifier == 0.5 then
            _G.speedModifier = _G.speedModifier + 0.5
        elseif _G.speedModifier < 10 then
            _G.speedModifier = _G.speedModifier + 1
        end
    elseif event == EVENT.NormalizeGameSpeed then
        _G.speedModifier = 1
    elseif event == EVENT.DecreaseGameSpeed then
        if _G.speedModifier <= 1 then
            _G.speedModifier = _G.speedModifier - 0.5
        else
            _G.speedModifier = _G.speedModifier - 1
        end
        if _G.speedModifier < 0.5 then
            _G.speedModifier = 0.5
        end
    elseif event == EVENT.Escape then
        if _G.BuildController.start then
            if loveframes.GetState() ~= states.STATE_PAUSE_MENU then
                loveframes.SetState(states.STATE_PAUSE_MENU)
                loveframes.TogglePause()
                ActionBar:showGroup("start")
                return
            end
            if loveframes.GetState() == states.STATE_INGAME_CONSTRUCTION then
                loveframes.SetState(states.STATE_INGAME_CONSTRUCTION)
                loveframes.TogglePause()
                return
            end
        elseif (loveframes.GetState() == states.STATE_PAUSE_MENU or
                loveframes.GetState() == states.STATE_INGAME_CONSTRUCTION) then
            if _G.BuildController.active and not _G.BuildController.start then
                ActionBar:unselectAll()
                _G.BuildController:disable()
                return
            end
            if _G.DestructionController.active then
                -- unselect the demolish button
                ActionBar:unselectAll()
                _G.DestructionController:disable()
                return
            end
            if ActionBar.currentGroup ~= "main" then
                if not _G.BuildController.start then
                    ActionBar:showGroup("main")
                end
                return
            end
        elseif (loveframes.GetState() == states.STATE_MARKET or
                loveframes.GetState() == states.STATE_STOCKPILE or
                loveframes.GetState() == states.STATE_GRANARY or
                loveframes.GetState() == states.STATE_MARKET_MAIN or
                loveframes.GetState() == states.STATE_KEEP_TAX or
                loveframes.GetState() == states.STATE_ARMOURY or
                loveframes.GetState() == states.STATE_UNITS) then
            ActionBar:switchMode()
            return
        end
        loveframes.TogglePause()
    elseif event == EVENT.ToggleDebugView then
        _G.DebugView:toggle()
    elseif event == EVENT.CenterViewToKeep then
        _G.state.viewXview = _G.IsoToScreenX(_G.state.keepX, _G.state.keepY)
        _G.state.viewYview = _G.IsoToScreenY(_G.state.keepX, _G.state.keepY)
    elseif event == EVENT.CenterViewToGranary then
        _G.state.viewXview = _G.IsoToScreenX(_G.state.granaryX, _G.state.granaryY)
        _G.state.viewYview = _G.IsoToScreenY(_G.state.granaryX, _G.state.granaryY)
    end
end

function game:mousereleased(x, y, button, istouch)
    -- TODO: Check if event is consumed
    if _G.Commander:mousereleased(x, y, button) then
        return
    end
    loveframes.mousereleased(x, y, button)
    _G.BrushController:mousereleased(button)
end

function game:wheelmoved(x, y)
    if scrollCountDown == 0 then
        scrollCountDown = 0.05
        scrolledAmountWithinShortPeriod = y
    else
        scrolledAmountWithinShortPeriod = scrolledAmountWithinShortPeriod + y
    end
end

function game:keyreleased(key, scancode)
    if console.isEnabled() then
        return
    end
    -- if not _G.BuildController.start then
    if key == "b" then
        _G.BrushController:toggleBuilding()
    elseif key == "delete" then
        _G.DestructionController:toggle()
    end

    if _G.BrushController:activated() then
        _G.BrushController:keyReleased(key)
    end
    -- end
end

return game
