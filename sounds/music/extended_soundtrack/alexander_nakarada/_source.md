All tracks were made by Alexander Nakarada (www.serpentsoundstudios.com)
Licensed under Creative Commons BY Attribution 4.0 License
https://creativecommons.org/licenses/by/4.0/

---

Title: Alexander Nakarada - Bonfire
Source URI: https://static1.squarespace.com/static/59dcec2112abd985b7417cbd/t/5a25b4c1652deaed1df37058/1512420552962/Bonfire+-+320bit.mp3/original/Bonfire+-+320bit.mp3?download=true
Source website: https://www.serpentsoundstudios.com/royalty-free-music/celtic-fantasy
Licence: CC BY 4.0

Title: Alexander Nakarada - Horsemen Approach (ft. Kevin MacLeod)
Source URI: https://static1.squarespace.com/static/59dcec2112abd985b7417cbd/t/61351d19a823c83c83d9ab40/1630870831856/Haven+-+320bit.mp3/original/Haven+-+320bit.mp3?download=true
Source website: https://www.serpentsoundstudios.com/royalty-free-music/celtic-fantasy
Licence: CC BY 4.0

Title: Alexander Nakarada - Tavern Loop One
Source URI: https://static1.squarespace.com/static/59dcec2112abd985b7417cbd/t/5ee8a7ef7507a723fc30e3b6/1592305677949/Tavern+Loop+One+-+320bit.mp3/original/Tavern+Loop+One+-+320bit.mp3?download=true
Source website: https://www.serpentsoundstudios.com/royalty-free-music/celtic-fantasy
Licence: CC BY 4.0
